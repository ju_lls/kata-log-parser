package jge;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

class CountDescComparatorTest {

    @Test
    void compareSameIdWithDifferentCount() {
        final CountDescComparator countDescComparator = new CountDescComparator();
        final IdWithCount idWithCountAs2 = new IdWithCount(1, 2);
        final IdWithCount idWithCountAs3 = new IdWithCount(1, 3);
        Assertions.assertTrue(countDescComparator.compare(idWithCountAs3, idWithCountAs2) < 0);
        Assertions.assertTrue(countDescComparator.compare(idWithCountAs2, idWithCountAs3) > 0);
    }

    @Test
    void compareSortResult() {
        final CountDescComparator countDescComparator = new CountDescComparator();
        final IdWithCount idWithCountAs2 = new IdWithCount(1, 2);
        final IdWithCount idWithCountAs3 = new IdWithCount(1, 3);
        final List<IdWithCount> idWithCountList = new ArrayList<>();
        idWithCountList.add(idWithCountAs2);
        idWithCountList.add(idWithCountAs3);
        idWithCountList.sort(countDescComparator);
        Assertions.assertEquals(idWithCountAs3, idWithCountList.get(0));// /!\ equals de reference
        Assertions.assertEquals(idWithCountAs2, idWithCountList.get(1));// /!\ equals de reference
    }

}