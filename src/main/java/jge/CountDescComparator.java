package jge;

import java.util.Comparator;

public class CountDescComparator implements Comparator<IdWithCount> {

    @Override
    public int compare(IdWithCount idWithCount1, IdWithCount idWithCount2) {
        return Integer.compare(idWithCount2.count, idWithCount1.count); //Inversion des ordres pour obtenir le tri décroissant
    }
}
