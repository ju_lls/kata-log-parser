package jge;

import java.util.function.Function;
import java.util.regex.Pattern;

public class LogLineParser implements Function<String, Integer> {

    final Pattern idPattern = Pattern.compile(".*id=(\\d+).*");

    @Override
    public Integer apply(String s) {
        return extractIdFromLine(s);
    }

    Integer extractIdFromLine(String line){
        final var matcher = idPattern.matcher(line);
        matcher.matches();
        return Integer.valueOf(matcher.group(1));
    }

}
