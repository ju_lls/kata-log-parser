package jge;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class LogArchiveParser {

    private final String archivePath;
    private final LogLineParser logLineParser;
    private final Map<Integer, Integer> idsMap;
    private final List<IdWithCount> idWithCountList;

    public static void main(String[] args) throws IOException {
        assert args.length > 1;
        LogArchiveParser logArchiveParser = new LogArchiveParser(args[0]);
        logArchiveParser.parseAndAggregate();
        logArchiveParser.orderResult();
        logArchiveParser.writeResult();
    }

    private LogArchiveParser(final String archivePath) {
        this.archivePath = archivePath;
        logLineParser = new LogLineParser();
        idsMap = new HashMap();
        idWithCountList = new ArrayList<>();
    }

    private Map<Integer, Integer> getIdsMap(){
        return idsMap;
    }

    private void parseAndAggregate() throws IOException {
        try (var zipInputStream = new ZipInputStream(new FileInputStream(archivePath))) {
            ZipEntry zipEntry;
            while ((zipEntry = zipInputStream.getNextEntry()) != null) {
                if (!zipEntry.isDirectory())
                    Files.readAllLines(Paths.get(zipEntry.getName()))
                            .stream().map(logLineParser)
                            .collect(Collectors.toMap(id -> id,
                                    id -> 1,
                                    (nbIteration1, incremental) -> nbIteration1 + incremental, this::getIdsMap));
            }
        }
    }

    private void orderResult() {
        idsMap.forEach((id, count) -> idWithCountList.add(new IdWithCount(id, count)));
        idWithCountList.sort(new CountDescComparator());
    }

    private void writeResult() {
        idWithCountList.stream().limit(5).forEachOrdered(System.out::println);
    }
}
